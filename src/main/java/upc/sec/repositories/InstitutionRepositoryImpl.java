package upc.sec.repositories;

import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import upc.sec.entities.Institution;
import upc.sec.entities.QInstitution;

import java.util.List;

public class InstitutionRepositoryImpl extends QuerydslRepositorySupport implements BasicEntityRepository<Institution> {

    private static final QInstitution qInstitution = QInstitution.institution;

    public InstitutionRepositoryImpl() {
        super(Institution.class);
    }

    @Override
    public Institution findByID(Integer id) {

        return from(qInstitution).where(qInstitution.id.eq(Long.valueOf(id))).select(qInstitution).fetchOne();
    }

    @Override
    public List<Institution> findAll(Predicate predicate) {
        return from(qInstitution).where(predicate).select(qInstitution).fetch();
    }
}
