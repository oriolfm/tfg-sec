package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upc.sec.entities.Institution;

@Repository
public interface InstitutionRepository extends JpaRepository<Institution, Long> {


}