package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upc.sec.entities.Cuarterao;
import upc.sec.entities.PlaceReference;

@Repository
public interface CuarteraoRepository extends JpaRepository<Cuarterao, Integer> {

}