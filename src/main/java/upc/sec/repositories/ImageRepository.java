package upc.sec.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import upc.sec.entities.SecImage;

public interface ImageRepository extends JpaRepository<SecImage, Long> {
    Optional<SecImage> findByName(String name);
}