package upc.sec.repositories;

import java.util.List;

import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

@Service
public interface BasicEntityRepository<T> {
    T findByID(Integer id);

    List<T> findAll(Predicate predicate);

}
