package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upc.sec.entities.SecFile;

@Repository
public interface FileRepository extends JpaRepository<SecFile, Long>{

}