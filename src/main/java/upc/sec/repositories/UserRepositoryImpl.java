package upc.sec.repositories;

import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import upc.sec.entities.SecUser;
import upc.sec.entities.QSecUser;

import java.util.List;

public class UserRepositoryImpl extends QuerydslRepositorySupport implements BasicEntityRepository<SecUser> {
    private static final QSecUser qUser = QSecUser.secUser;

    public UserRepositoryImpl() {
        super(SecUser.class);
    }

    @Override
    public SecUser findByID(Integer id) {
        return from(qUser).where(qUser.id.eq(id)).select(qUser).fetchOne();
    }

    @Override
    public List<SecUser> findAll(Predicate predicate) {
        return from(qUser).where(predicate).select(qUser).fetch();
    }
}
