package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upc.sec.entities.Severity;

@Repository
public interface SeverityRepository extends JpaRepository<Severity, Integer> {

}