package upc.sec.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import upc.sec.entities.SecUser;

@Repository
public interface UserRepository extends CrudRepository<SecUser, Integer> {
    SecUser findByUsername(String username);
}
