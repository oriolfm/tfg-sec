package upc.sec.repositories;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.DateTimePath;
import upc.sec.entities.Incidence;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import upc.sec.entities.QIncidence;

public class IncidenceRepositoryImpl extends QuerydslRepositorySupport implements BasicEntityRepository<Incidence> {

    private static final QIncidence qIncidence = QIncidence.incidence;

    public IncidenceRepositoryImpl() {
        super(Incidence.class);
    }

    @Override
    public Incidence findByID(Integer id) {

        return from(qIncidence).where(qIncidence.id.eq(Long.valueOf(id))).select(qIncidence).fetchOne();
    }

    @Override
    public List<Incidence> findAll(Predicate predicate) {
        return from(qIncidence).where(predicate).select(qIncidence).fetch();
    }


    public List<Incidence> findAllBetweenDates(Predicate predicate, Timestamp after, Timestamp before) {
        return from(qIncidence).where(predicate, qIncidence.creationDate.after(after),qIncidence.creationDate.before(before)).select(qIncidence).fetch();
    }
}
