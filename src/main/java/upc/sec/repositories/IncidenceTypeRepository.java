package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upc.sec.entities.IncidenceType;

@Repository
public interface IncidenceTypeRepository extends JpaRepository<IncidenceType, Long> {

}