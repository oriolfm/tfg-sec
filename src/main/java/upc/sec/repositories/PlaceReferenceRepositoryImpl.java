package upc.sec.repositories;

import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import upc.sec.entities.PlaceReference;
import upc.sec.entities.QPlaceReference;

import java.util.List;

public class PlaceReferenceRepositoryImpl extends QuerydslRepositorySupport implements BasicEntityRepository<PlaceReference> {
    private static final QPlaceReference qPlaceReference = QPlaceReference.placeReference;

    public PlaceReferenceRepositoryImpl() {
        super(PlaceReference.class);
    }

    @Override
    public PlaceReference findByID(Integer id) {
        return from(qPlaceReference).where(qPlaceReference.id.eq(id)).select(qPlaceReference).fetchOne();
    }

    @Override
    public List<PlaceReference> findAll(Predicate predicate) {
        return from(qPlaceReference).where(predicate).select(qPlaceReference).fetch();
    }
}
