package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import upc.sec.entities.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

}