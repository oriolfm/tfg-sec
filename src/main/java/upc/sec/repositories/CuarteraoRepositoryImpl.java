package upc.sec.repositories;

import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import upc.sec.entities.Cuarterao;
import upc.sec.entities.QCuarterao;
import upc.sec.entities.QSecUser;
import upc.sec.entities.SecUser;

import java.util.List;

public class CuarteraoRepositoryImpl extends QuerydslRepositorySupport implements BasicEntityRepository<Cuarterao> {
    private static final QCuarterao qCuarterao = QCuarterao.cuarterao;

    public CuarteraoRepositoryImpl() {
        super(Cuarterao.class);
    }

    @Override
    public Cuarterao findByID(Integer id) {
        return from(qCuarterao).where(qCuarterao.id.eq(id)).select(qCuarterao).fetchOne();
    }

    @Override
    public List<Cuarterao> findAll(Predicate predicate) {
        return from(qCuarterao).where(predicate).select(qCuarterao).fetch();
    }
}
