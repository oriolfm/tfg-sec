package upc.sec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import upc.sec.entities.Comment;

import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Long> {


}