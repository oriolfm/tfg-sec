package upc.sec.auth.model;


import upc.sec.entities.SecUser;

import java.io.Serializable;
public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private final SecUser user;
    public JwtResponse(String jwttoken, SecUser user) {
        this.jwttoken = jwttoken;
        this.user = user;
    }
    public String getToken() {
        return this.jwttoken;
    }
    public SecUser getUser(){ return this.user;}
}