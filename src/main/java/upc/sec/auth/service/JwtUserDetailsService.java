package upc.sec.auth.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import upc.sec.dto.UserDTO;
import upc.sec.entities.Institution;
import upc.sec.entities.SecImage;
import upc.sec.entities.SecUser;
import upc.sec.entities.Severity;
import upc.sec.repositories.ImageRepository;
import upc.sec.repositories.InstitutionRepository;
import upc.sec.repositories.UserRepository;

import javax.swing.text.html.Option;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InstitutionRepository institutionRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public static SecUser contextUser;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<SecUser> users = (List<SecUser>) userRepository.findAll();
        Optional<SecUser> user = users.stream().filter(u -> u.getUsername().equals(username)).findFirst();
        if (user.isPresent()) {
            return new User(user.get().getUsername(), user.get().getPassword(),
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

    public SecUser findByUsername(String username){
        List<SecUser> users = (List<SecUser>) userRepository.findAll();
        Optional<SecUser> user = users.stream().filter(u -> u.getUsername().equals(username)).findFirst();
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

    public void setContextUser(SecUser user){
        contextUser = user;
    }

    public SecUser getContextUser(){
        return contextUser;
    }

    public SecUser save(UserDTO userDto) throws Exception {
        //if(contextUser.getSuperAdmin() ) { VERIFYING PERMISSION TO REGISTER
            SecUser newUser = new SecUser();
            if(userDto.getId() != null){
                Optional<SecUser> optUser = userRepository.findById(userDto.getId());
                newUser = optUser.orElse(new SecUser());
                newUser.setId(userDto.getId());
            }
            newUser.setUsername(userDto.getUsername());
            if(userDto.getPassword() != null)
                newUser.setPassword(bcryptEncoder.encode(userDto.getPassword()));
            newUser.setEmail(userDto.getEmail());
            if (userDto.getInstitutionId() != null) {
                Optional<Institution> inst = institutionRepository.findById(Long.valueOf(userDto.getInstitutionId()));
                newUser.setInstitution(inst.orElse(null));
            }
            if(userDto.getImageId() != null){
                Optional<SecImage> image = imageRepository.findById(Long.valueOf(userDto.getImageId()));
                newUser.setImage(image.orElse(null));
            }
            if (userDto.getSuperAdmin() == null) {
                newUser.setSuperAdmin(false);
            } else {
                newUser.setSuperAdmin(userDto.getSuperAdmin());
            }
            return userRepository.save(newUser);
       // }
       // throw new Exception("ONLY SUPERADMINS CAN REGISTER NEW USERS");
    }
}