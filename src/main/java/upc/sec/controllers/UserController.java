package upc.sec.controllers;

import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import upc.sec.auth.service.JwtUserDetailsService;
import upc.sec.dto.IncidenceTypeDTO;
import upc.sec.dto.UserDTO;
import upc.sec.entities.IncidenceType;
import upc.sec.entities.SecUser;
import upc.sec.exceptions.IncidenceNotFoundException;
import upc.sec.exceptions.UserNotFoundException;
import upc.sec.repositories.UserRepository;
import upc.sec.repositories.UserRepositoryImpl;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class UserController {

    @Autowired
    UserRepository repository;

    @Autowired
    UserRepositoryImpl repositoryImpl;

    @Autowired
    JwtUserDetailsService service;

    @GetMapping("/users")
    public List<SecUser> getIncidences(@QuerydslPredicate(root = SecUser.class) Predicate predicate) {

        return repositoryImpl.findAll(predicate);
    }

    @PutMapping("/users/edit")
    public SecUser editUser(@RequestBody UserDTO userDTO) throws Exception {
        return service.save(userDTO);
    }

    @DeleteMapping("/users/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Integer userId) throws UserNotFoundException{
        SecUser user = repository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        repository.delete(user);

        return ResponseEntity.ok().build();
    }
}
