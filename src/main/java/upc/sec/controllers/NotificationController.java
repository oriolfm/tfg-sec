package upc.sec.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import upc.sec.dto.NotificationDTO;
import upc.sec.entities.IncidenceType;
import upc.sec.entities.Severity;
import upc.sec.entities.State;
import upc.sec.repositories.IncidenceTypeRepository;
import upc.sec.repositories.SeverityRepository;
import upc.sec.repositories.StateRepository;
import upc.sec.services.NotificationService;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class NotificationController {

    @Autowired
    NotificationService service;
/*
    @RequestMapping(value = "/notify", method = RequestMethod.POST)
    public ResponseEntity<?> sendNotification(@RequestBody NotificationDTO notification) throws MessagingException {
        service.sendNotification(notification);
        return  ResponseEntity.ok().build();
    }*/

}