package upc.sec.controllers;

import java.io.IOException;
import java.util.Base64;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import upc.sec.entities.SecImage;
import upc.sec.repositories.ImageRepository;

@RestController
@RequestMapping(path = "image")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class ImageUploadController {

    @Autowired
    ImageRepository imageRepository;

    @PostMapping("/upload")
    public SecImage uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        byte[] encoded = Base64.getEncoder().encode(file.getBytes());
        SecImage img = new SecImage(file.getOriginalFilename(), file.getContentType(),
                encoded);
        img = imageRepository.save(img);
        img.setPicByte(Base64.getDecoder().decode(img.getPicByte()));
        return img;
    }

    @GetMapping(path = { "/get/{id}" })
    public SecImage getImage(@PathVariable("id") String imageId) throws IOException {
        final Optional<SecImage> retrievedImage = imageRepository.findById(Long.valueOf(imageId));
        SecImage img = retrievedImage.get();
        img.setPicByte(Base64.getDecoder().decode(img.getPicByte()));
        return img;
    }

}