package upc.sec.controllers;

import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.*;
import upc.sec.entities.Comment;
import upc.sec.entities.SecUser;
import upc.sec.repositories.CommentRepository;
import upc.sec.repositories.UserRepositoryImpl;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class CommentsController {

    @Autowired
    CommentRepository repository;

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public Comment postComment(@RequestBody String comment) {
        Comment newComm = new Comment();
        newComm.setCommentary(comment);
        return repository.save(newComm);
    }
}
