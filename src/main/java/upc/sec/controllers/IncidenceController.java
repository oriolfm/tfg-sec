package upc.sec.controllers;

import com.querydsl.core.types.Predicate;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import upc.sec.dto.IncidenceDTO;
import upc.sec.entities.Incidence;
import upc.sec.exceptions.IncidenceNotFoundException;
import upc.sec.repositories.IncidenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import upc.sec.services.IncidenceService;

import java.text.ParseException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class IncidenceController {

    @Autowired
    IncidenceRepository repository;

    @Autowired
    IncidenceService service;

    @GetMapping("/incidences")
    public List<Incidence> getIncidences( @QuerydslPredicate(root = Incidence.class) Predicate predicate, @RequestParam (required=false) String after, @RequestParam (required=false) String before) throws ParseException {
        return service.findAll(predicate, after, before);
    }

    @RequestMapping(value = "/incidences/new", method = RequestMethod.POST)
    public Incidence createIncidence(@RequestBody IncidenceDTO incidence) {
        return service.save(incidence);
    }

    @GetMapping("/incidences/{id}")
    public Incidence getIncidenceById(@PathVariable(value = "id") Long incidenceId) throws IncidenceNotFoundException {
       return service.findById(incidenceId);
       // return repository.findById(incidenceId)
       //         .orElseThrow(() -> new IncidenceNotFoundException(incidenceId));
    }

    @PutMapping("/incidences/edit")
    public Incidence updateIncidence(@RequestBody IncidenceDTO incidenceDetails) throws IncidenceNotFoundException {
        return service.updateIncidence(Long.valueOf(incidenceDetails.getId()), incidenceDetails);
    }

    // Delete a Incidence
    @DeleteMapping("/incidences/delete/{id}")
    public ResponseEntity<?> deleteIncidence(@PathVariable(value = "id") Long incidenceId) throws IncidenceNotFoundException {
        Incidence incidence = repository.findById(incidenceId)
                .orElseThrow(() -> new IncidenceNotFoundException(incidenceId));

        repository.delete(incidence);

        return ResponseEntity.ok().build();
    }
}