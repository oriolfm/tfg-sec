package upc.sec.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import upc.sec.dto.InstitutionDTO;
import upc.sec.entities.Institution;
import upc.sec.exceptions.InstitutionNotFoundException;
import upc.sec.repositories.InstitutionRepository;
import upc.sec.services.IncidenceService;
import upc.sec.services.InstitutionService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class  InstitutionController {

    @Autowired
    InstitutionRepository repository;

    @Autowired
    InstitutionService service;

    // Get all institutions
    @GetMapping("/institutions")
    public List<Institution> getInstitutions() {
        return service.findAll();
    }

    // Create a new institution
    @RequestMapping(value = "/institutions/new", method = RequestMethod.POST)
    public Institution createInstitution(@RequestBody InstitutionDTO institution) {
        return service.save(institution);
    }

    // Get a Single Institution
    @GetMapping("/institutions/{id}")
    public Institution getInstitutionById(@PathVariable(value = "id") Long institutionId) throws InstitutionNotFoundException {
        return repository.findById(institutionId)
                .orElseThrow(() -> new InstitutionNotFoundException(institutionId));
    }

    @PutMapping("/institutions/edit")
    public Institution updateInstitution(@Valid @RequestBody InstitutionDTO institutionDetails) throws InstitutionNotFoundException {

        return service.updateInstitution(Long.valueOf(institutionDetails.getId()), institutionDetails);
    }

    // Delete a Institution
    @DeleteMapping("/institutions/delete/{id}")
    public ResponseEntity<?> deleteInstitution(@PathVariable(value = "id") Long institutionId) throws InstitutionNotFoundException {
        Institution institution = repository.findById(institutionId)
                .orElseThrow(() -> new InstitutionNotFoundException(institutionId));

        repository.delete(institution);

        return ResponseEntity.ok().build();
    }
}