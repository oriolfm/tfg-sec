package upc.sec.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import upc.sec.entities.SecFile;
import upc.sec.repositories.FileRepository;
import upc.sec.repositories.FileRepository;

import java.io.IOException;
import java.util.Base64;
import java.util.Optional;

@RestController
@RequestMapping(path = "file")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class FileUploadController {

    @Autowired
    FileRepository fileRepository;

    @PostMapping("/upload")
    public SecFile uplaodFile(@RequestParam("file") MultipartFile inFile) throws IOException {
        System.out.println("Original File Byte Size - " + inFile.getBytes().length);
        byte[] encoded = Base64.getEncoder().encode(inFile.getBytes());
        SecFile file = new SecFile(inFile.getOriginalFilename(), inFile.getContentType(),
                encoded);
        file = fileRepository.save(file);
        file.setPicByte(Base64.getDecoder().decode(file.getPicByte()));
        return file;
    }

    @GetMapping(path = { "/get/{id}" })
    public SecFile getFile(@PathVariable("id") String imageId) throws IOException {
        final Optional<SecFile> retrievedFile = fileRepository.findById(Long.valueOf(imageId));
        SecFile file = retrievedFile.get();
        file.setPicByte(Base64.getDecoder().decode(file.getPicByte()));
        return file;
    }

}