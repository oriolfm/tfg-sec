package upc.sec.controllers;

import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import upc.sec.dto.*;
import upc.sec.entities.*;
import upc.sec.exceptions.UserNotFoundException;
import upc.sec.repositories.*;
import upc.sec.services.FiltersService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class FiltersController {

    @Autowired
    StateRepository stateRepository;

    @Autowired
    IncidenceTypeRepository typeRepository;

    @Autowired
    PlaceReferenceRepository placeRepository;

    @Autowired
    PlaceReferenceRepositoryImpl placeRepositoryImpl;

    @Autowired
    CuarteraoRepository cuarteraoRepository;

    @Autowired
    CuarteraoRepositoryImpl cuarteraoRepositoryImpl;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    SeverityRepository severityRepository;

    @Autowired
    FiltersService service;

    @GetMapping("/states")
    public List<State> getStates() {
        return stateRepository.findAll();
    }

    @PostMapping("/states/new")
    public State saveState(@RequestBody StateDTO stateDTO){
        return service.saveState(stateDTO);
    }

    @PutMapping("/states/edit")
    public State editState(@RequestBody StateDTO stateDTO){
        return service.saveState(stateDTO);
    }

    @DeleteMapping("/states/delete/{id}")
    public ResponseEntity<?> deleteState(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        if(id == 0 || id == 4) return ResponseEntity.badRequest().build();
        State state = stateRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
        stateRepository.delete(state);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/incidence-types")
    public List<IncidenceType> getTypes() {
        return typeRepository.findAll();
    }

    @PostMapping("/incidence-types/new")
    public IncidenceType saveType(@RequestBody IncidenceTypeDTO typeDTO){
        return service.saveType(typeDTO);
    }

    @PutMapping("/incidence-types/edit")
    public IncidenceType editType(@RequestBody IncidenceTypeDTO typeDTO){
        return service.saveType(typeDTO);
    }

    @DeleteMapping("/incidence-types/delete/{id}")
    public ResponseEntity<?> deleteType(@PathVariable(value = "id") Long id) throws EntityNotFoundException {
        IncidenceType type = typeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
        typeRepository.delete(type);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/places")
    public List<PlaceReference> getPlaces( @QuerydslPredicate(root = PlaceReference.class) Predicate predicate) {
        return placeRepositoryImpl.findAll(predicate);
    }

    @PostMapping("/places/new")
    public PlaceReference savePlace(@RequestBody PlaceReferenceDTO placeDTO){
        return service.savePlace(placeDTO);
    }

    @PutMapping("/places/edit")
    public PlaceReference editPlace(@RequestBody PlaceReferenceDTO placeDTO){
        return service.savePlace(placeDTO);
    }

    @DeleteMapping("/places/delete/{id}")
    public ResponseEntity<?> deletePlace(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        PlaceReference place = placeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
        placeRepository.delete(place);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/cuarteraos")
    public List<Cuarterao> getCuarteraos(@QuerydslPredicate(root = Cuarterao.class) Predicate predicate) {
        return cuarteraoRepositoryImpl.findAll(predicate);
    }

    @PostMapping("/cuarteraos/new")
    public Cuarterao saveCuarteraos(@RequestBody CuarteraoDTO dto){
        return service.saveCuarterao(dto);
    }

    @PutMapping("/cuarteraos/edit")
    public Cuarterao editCuarteraos(@RequestBody CuarteraoDTO dto){
        return service.saveCuarterao(dto);
    }

    @DeleteMapping("/cuarteraos/delete/{id}")
    public ResponseEntity<?> deleteCuarterao(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        Cuarterao place = cuarteraoRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
        cuarteraoRepository.delete(place);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/districts")
    public List<District> getDistricts() {
        return districtRepository.findAll();
    }

    @PostMapping("/districts/new")
    public District saveDistrict(@RequestBody DistrictDTO dto){
        return service.saveDistrict(dto);
    }

    @PutMapping("/districts/edit")
    public District editDistrict(@RequestBody DistrictDTO dto){

        return service.saveDistrict(dto);
    }

    @DeleteMapping("/districts/delete/{id}")
    public ResponseEntity<?> deleteDistrict(@PathVariable(value = "id") Integer id) throws EntityNotFoundException {
        District place = districtRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
        districtRepository.delete(place);

        return ResponseEntity.ok().build();
    }
    @GetMapping("/severities")
    public List<Severity> getSeverities() {
        return severityRepository.findAll();
    }

}