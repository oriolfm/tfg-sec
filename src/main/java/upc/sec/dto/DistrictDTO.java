package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistrictDTO {
    private String name;
    private Integer id;
}