package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NotificationDTO {

    private List<String> emails;

    private String message;

}
