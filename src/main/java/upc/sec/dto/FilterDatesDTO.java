package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.ArrayList;

@Getter
@Setter
public class FilterDatesDTO {

    private Timestamp after;

    private Timestamp before;

}
