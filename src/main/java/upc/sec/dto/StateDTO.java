package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StateDTO {
    private String name;
    private Integer id;
    private String color;
}