package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
    private Integer id;
    private String username;
    private String password;
    private String email;
    private Boolean superAdmin;
    private Integer institutionId;
    private Integer imageId;
}