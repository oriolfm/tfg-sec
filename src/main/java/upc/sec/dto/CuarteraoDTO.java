package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuarteraoDTO {
    private String name;
    private Integer districtId;
    private Integer id;
}