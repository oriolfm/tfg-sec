package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlaceReferenceDTO {
    private String name;
    private String coordinates;
    private Integer cuarteraoId;
    private Integer id;
}