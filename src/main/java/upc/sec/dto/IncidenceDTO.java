package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;
import upc.sec.entities.Incidence;

import java.util.ArrayList;

@Getter
@Setter
public class IncidenceDTO {

    private Integer id;

    private Integer stateId;

    private Integer typeId;

    private String descriptionText;

    private String coordinates;

    private Integer severityId;

    private String email;

    private String phoneNumber;

    private String photo;

    private ArrayList<Integer> files;

    private ArrayList<Integer> images;

    private ArrayList<Integer> comments;

    private Integer placeId;

}
