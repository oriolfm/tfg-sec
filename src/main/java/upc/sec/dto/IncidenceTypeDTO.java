package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncidenceTypeDTO {
    private String name;
    private Integer institutionId;
    private Integer id;
}