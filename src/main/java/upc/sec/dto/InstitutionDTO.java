package upc.sec.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class InstitutionDTO {

    private Integer id;

    private String name;

    private String email;

    private String phoneNumber;

    private List<Integer> competences;
}
