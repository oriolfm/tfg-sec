package upc.sec.exceptions;

public class UserNotFoundException extends Exception{

    public UserNotFoundException(Integer userId){
        super(String.format("User is not found with id : '%s'", userId));
    }
}
