package upc.sec.exceptions;

public class InstitutionNotFoundException extends Exception{
    private long institutionId;

    public InstitutionNotFoundException(long institutionId){
        super(String.format("Institution is not found with id : '%s'", institutionId));
    }
}
