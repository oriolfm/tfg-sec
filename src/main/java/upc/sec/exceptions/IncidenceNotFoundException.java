package upc.sec.exceptions;

public class IncidenceNotFoundException extends Exception{
    private long incidenceId;

    public IncidenceNotFoundException (long incidenceId){
        super(String.format("Incidence is not found with id : '%s'", incidenceId));
    }
}
