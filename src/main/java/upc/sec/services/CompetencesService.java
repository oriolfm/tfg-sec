package upc.sec.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import upc.sec.auth.service.JwtUserDetailsService;
import upc.sec.dto.IncidenceTypeDTO;
import upc.sec.entities.IncidenceType;
import upc.sec.entities.Institution;
import upc.sec.entities.SecUser;
import upc.sec.repositories.IncidenceTypeRepository;
import upc.sec.repositories.InstitutionRepository;
import upc.sec.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompetencesService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IncidenceTypeRepository typeRepository;

    @Autowired
    private InstitutionRepository institutionRepository;

    @Autowired
    private JwtUserDetailsService userService;


    public List<IncidenceType> getCompetences() {
        SecUser user = userService.contextUser;
        if (user.getSuperAdmin()) {
            return typeRepository.findAll();
        } else if (user.getInstitution() != null) {
            ArrayList<IncidenceType> competences = new ArrayList<>();
            competences.addAll(user.getInstitution().getCompetences());
            return competences;
        }
        return null;
    }

    public Boolean isSuperAdmin(){
        return userService.contextUser.getSuperAdmin();
    }

    public IncidenceTypeDTO getTypeDTO (IncidenceType type){
        IncidenceTypeDTO dto = new IncidenceTypeDTO();
        dto.setId(type.getId().intValue());
        dto.setInstitutionId(type.getEmergencyInstitutionId());
        dto.setName(type.getName());
        return dto;
    }
    public List<IncidenceTypeDTO> getTypesDTO (List<IncidenceType> types){
        List<IncidenceTypeDTO> dtos = new ArrayList<>();
        for(IncidenceType type : types){
            dtos.add(getTypeDTO(type));
        }
        return dtos;
    }
}
