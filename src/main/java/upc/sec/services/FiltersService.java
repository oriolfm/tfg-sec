package upc.sec.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import upc.sec.dto.*;
import upc.sec.entities.*;
import upc.sec.exceptions.InstitutionNotFoundException;
import upc.sec.repositories.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class FiltersService {

    @Autowired
    private InstitutionRepository institutionRepository;

    @Autowired
    private InstitutionRepositoryImpl institutionRepositoryImpl;

    @Autowired
    private IncidenceTypeRepository typeRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private CuarteraoRepository cuarteraoRepository;

    @Autowired
    private PlaceReferenceRepository placeRepository;


    @Autowired
    private StateRepository stateRepository;

    public State saveState(StateDTO dto){
        State state = new State();
        state.setStateName(dto.getName());
        state.setId(dto.getId());
        state.setColor(dto.getColor());
        return stateRepository.save(state);
    }

    public IncidenceType saveType(IncidenceTypeDTO typeDTO){
        IncidenceType type = new IncidenceType();
        type.setName(typeDTO.getName());
        type.setId(Long.valueOf(typeDTO.getId()));
        type.setEmergencyInstitutionId(typeDTO.getInstitutionId());
        return typeRepository.save(type);
    }

    public PlaceReference savePlace(PlaceReferenceDTO placeReferenceDTO){
        PlaceReference place = new PlaceReference();
        place.setName(placeReferenceDTO.getName());
        place.setId(placeReferenceDTO.getId());
        place.setCoordinates(placeReferenceDTO.getCoordinates());
        Optional<Cuarterao> cuarterao = cuarteraoRepository.findById(placeReferenceDTO.getCuarteraoId());
        place.setCuarterao(cuarterao.orElseGet(null));
        return placeRepository.save(place);
    }

    public Cuarterao saveCuarterao(CuarteraoDTO dto){
        Cuarterao cuart = new Cuarterao();
        cuart.setName(dto.getName());
        cuart.setId(dto.getId());
        Optional<District> district = districtRepository.findById(dto.getDistrictId());
        cuart.setDistrict(district.orElseGet(null));
        return cuarteraoRepository.save(cuart);
    }

    public District saveDistrict(DistrictDTO dto){
        District dist = new District();
        dist.setName(dto.getName());
        dist.setId(dto.getId());
        return districtRepository.save(dist);
    }
}
