package upc.sec.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import upc.sec.dto.IncidenceDTO;
import upc.sec.entities.*;
import upc.sec.exceptions.IncidenceNotFoundException;
import upc.sec.repositories.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.querydsl.core.types.Predicate;

@Component
public class IncidenceService {


    @Autowired
    private IncidenceRepository incidenceRepository;

    @Autowired
    private IncidenceTypeRepository typeRepository;

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private SeverityRepository severityRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PlaceReferenceRepository placeRepository;

    @Autowired
    private IncidenceRepositoryImpl incidenceRepositoryImpl;


    public Incidence save(IncidenceDTO dto){
        Incidence newIncidence = dtoToEntity(dto);
        return incidenceRepository.save(newIncidence);

    }

    private Incidence dtoToEntity(IncidenceDTO dto){
        Incidence entity = new Incidence();

        Timestamp now = new Timestamp(System.currentTimeMillis());
        entity.setLastUpdate(now);

        if(dto.getId() != null){
            entity.setId(Long.valueOf(dto.getId()));
            Optional<Incidence> optIncidence = incidenceRepository.findById(Long.valueOf(dto.getId()));
            Incidence incidence = optIncidence.get();
            if(incidence!=null){
                entity.setCreationDate(incidence.getCreationDate());
            }
        }
        else{
            entity.setCreationDate(now);
        }
        entity.setCoordinates(dto.getCoordinates());
        entity.setDescriptionText(dto.getDescriptionText());
        entity.setEmail(dto.getEmail());
        entity.setPhoneNumber(dto.getPhoneNumber());
        if(dto.getSeverityId()!=null){
            Optional<Severity> severity = severityRepository.findById(dto.getSeverityId());
            entity.setSeverity(severity.orElse(null));
        }
        if(dto.getTypeId()!=null) {
            Optional<IncidenceType> t = typeRepository.findById(Long.valueOf(dto.getTypeId()));
            entity.setType(t.orElse(null));
        }
        if(dto.getStateId()!=null) {
            Optional<State> s = stateRepository.findById(dto.getStateId());
            entity.setState(s.orElse(null));
        }
        else{
            Optional<State> s = stateRepository.findById(0); //pendiente verificacion
            entity.setState(s.orElse(null));
        }
        if(dto.getPlaceId()!=null) {
            Optional<PlaceReference> p = placeRepository.findById(dto.getPlaceId());
            entity.setPlace(p.orElse(null));
        }
        if(dto.getFiles()!= null){
            Set<SecFile> listFiles = new HashSet<SecFile>();
            for(Integer fileId : dto.getFiles()){
                Optional<SecFile> optImg = fileRepository.findById(Long.valueOf(fileId));
                SecFile file = optImg.orElse(null);
                if(file!=null) listFiles.add(file);
            }
            entity.setFiles(listFiles);
        }
        if(dto.getImages()!= null){
            Set<SecImage> listImages = new HashSet<SecImage>();
            for(Integer imageId : dto.getImages()){
                Optional<SecImage> optImg = imageRepository.findById(Long.valueOf(imageId));
                SecImage img = optImg.orElse(null);
                if(img!=null) listImages.add(img);
            }
            entity.setImages(listImages);
        }
        if(dto.getComments()!=null){
            Set<Comment> listComments = new HashSet<>();
            for(Integer commentId : dto.getComments()){
                Optional<Comment> optComment = commentRepository.findById(Long.valueOf(commentId));
                Comment comment = optComment.orElse(null);
                if(comment!=null) listComments.add(comment);
            }
            entity.setComments(listComments);
        }

        return entity;
    }

    public Incidence updateIncidence(Long incidenceId, IncidenceDTO incidenceDetails) throws IncidenceNotFoundException {

        Incidence editedIncidence = dtoToEntity(incidenceDetails);
        editedIncidence.setId(incidenceId);

        return incidenceRepository.save(editedIncidence);
    }

    public Incidence findById(Long incidenceId){
        return incidenceRepositoryImpl.findByID(incidenceId.intValue(), null);
    }

    public List<Incidence> findAll(Predicate predicate, String after, String before) throws ParseException {
        List<Incidence> incidences;
        if(after != null && before != null){
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = formatter.parse(after);
            Timestamp afterStamp = new Timestamp(date.getTime());
            date = formatter.parse(before);
            Timestamp beforeStamp = new Timestamp(date.getTime());
            incidences = incidenceRepositoryImpl.findAllBetweenDates(predicate, afterStamp, beforeStamp);
        }
        else
            incidences= incidenceRepositoryImpl.findAll(predicate);
        for(Incidence incidence : incidences){
            for(SecImage img : incidence.getImages()){
                img.setPicByte(Base64.getDecoder().decode(img.getPicByte()));
            }
            for(SecFile file : incidence.getFiles()){
                file.setPicByte(Base64.getDecoder().decode(file.getPicByte()));
            }
        }
        return incidences;
    }
}
