package upc.sec.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import upc.sec.dto.InstitutionDTO;
import upc.sec.entities.Institution;
import upc.sec.entities.IncidenceType;
import upc.sec.entities.State;
import upc.sec.exceptions.IncidenceNotFoundException;
import upc.sec.exceptions.InstitutionNotFoundException;
import upc.sec.repositories.InstitutionRepository;
import upc.sec.repositories.IncidenceTypeRepository;
import upc.sec.repositories.InstitutionRepositoryImpl;
import upc.sec.repositories.StateRepository;

import java.sql.Timestamp;
import java.util.*;

@Component
public class InstitutionService {

    @Autowired
    private InstitutionRepository institutionRepository;

    @Autowired
    private InstitutionRepositoryImpl institutionRepositoryImpl;

    @Autowired
    private IncidenceTypeRepository typeRepository;


    public Institution save(InstitutionDTO dto){
        Institution newInstitution = dtoToEntity(dto);
        return institutionRepository.save(newInstitution);

    }

    private Institution dtoToEntity(InstitutionDTO dto){
        Institution entity = new Institution();
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setPhoneNumber(dto.getPhoneNumber());

        if(dto.getCompetences()!=null) {
            Set<IncidenceType> competences = new HashSet<>();
            for(Integer typeId:dto.getCompetences()) {
                Optional<IncidenceType> t = typeRepository.findById(Long.valueOf(typeId));
                if(t.isPresent()) competences.add(t.get());
            }
            entity.setCompetences(competences);
        }

        return entity;
    }

    public Institution updateInstitution(Long institutionId, InstitutionDTO institutionDetails) throws InstitutionNotFoundException {
        Institution institution = institutionRepository.findById(institutionId)
                .orElseThrow(() -> new InstitutionNotFoundException(institutionId));

        Set<IncidenceType> competences = new HashSet<>();
        for (Integer typeId:institutionDetails.getCompetences()){
            Optional<IncidenceType> type = typeRepository.findById(Long.valueOf(typeId));
            IncidenceType t = type.orElse(null);
            if(t!=null) competences.add(t);
        }
        institution.setCompetences(competences);
        institution.setPhoneNumber(institutionDetails.getPhoneNumber());
        institution.setEmail(institutionDetails.getEmail());
        institution.setName(institutionDetails.getName());

        return institutionRepository.save(institution);
    }

    public List<Institution> findAll() {
        List<Institution> institutions = institutionRepository.findAll();
        return institutions;
    }
}
