package upc.sec.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "incidences")
public class Incidence {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private Timestamp creationDate;

    private Timestamp lastUpdate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_id")
    @Fetch(FetchMode.JOIN)
    private State state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id")
    @Fetch(FetchMode.JOIN)
    private IncidenceType type;

    private String descriptionText;

    private String coordinates;

    private String email;

    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "severity_id")
    @Fetch(FetchMode.JOIN)
    private Severity severity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place_id")
    @Fetch(FetchMode.JOIN)
    private PlaceReference place;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "incidence_files",
            joinColumns = @JoinColumn(name = "incidence_id"),
            inverseJoinColumns = @JoinColumn(name = "file_id"))
    private Set<SecFile> files;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "incidence_images",
            joinColumns = @JoinColumn(name = "incidence_id"),
            inverseJoinColumns = @JoinColumn(name = "image_id"))
    private Set<SecImage> images;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "incidence_comments",
            joinColumns = @JoinColumn(name = "incidence_id"),
            inverseJoinColumns = @JoinColumn(name = "comment_id"))
    private Set<Comment> comments;


}