package upc.sec.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "incidence_types")
public class IncidenceType {
    @Id
    private Long id;

    private String name;

    private Integer emergencyInstitutionId;
}
