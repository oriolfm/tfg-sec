package upc.sec.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "place_reference")
public class PlaceReference {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String coordinates;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cuarterao_id")
    @Fetch(FetchMode.JOIN)
    private Cuarterao cuarterao;
}
