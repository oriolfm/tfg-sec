package upc.sec.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "district")
public class District {
    @Id
    private Integer id;

    private String name;
}
