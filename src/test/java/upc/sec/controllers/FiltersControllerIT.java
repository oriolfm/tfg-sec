package upc.sec.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FiltersControllerIT {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getStates(){

        ResponseEntity<String> response = restTemplate.getForEntity("/states", String.class);

        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);

    }
    @Test
    public void getIncidenceTypes(){

        ResponseEntity<String> response = restTemplate.getForEntity("/incidence-types", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);

    }
    @Test
    public void getSeverities(){

        ResponseEntity<String> response = restTemplate.getForEntity("/severities", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);

    }


}
