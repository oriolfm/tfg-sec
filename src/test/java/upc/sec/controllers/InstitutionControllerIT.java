package upc.sec.controllers;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InstitutionControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getInstitutions(){

        ResponseEntity<String> response = restTemplate.getForEntity("/institutions", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);

        response = restTemplate.getForEntity("/institutions/1", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);
    }

}
