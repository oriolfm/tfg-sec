package upc.sec.controllers;


import com.fasterxml.jackson.databind.JsonNode;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import upc.sec.dto.IncidenceDTO;
import upc.sec.entities.Incidence;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IncidenceControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getIncidences(){

        ResponseEntity<String> response = restTemplate.getForEntity("/incidences", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);

    }

    @Test
    public void getIncidencesFiltered(){

        ResponseEntity<String> response = restTemplate.getForEntity("/incidences?state.id=3", String.class);
        System.out.println("--------GET: "+ response.getBody());
        assert(response.getStatusCode()).equals(HttpStatus.OK);

    }

    @Test
    public void postIncidences() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("coordinates","-25.959779;32.578845");
        jsonObject.put("email","test@test");
        jsonObject.put("severityId",1);
        jsonObject.put("descriptionText","testTEXT");
        jsonObject.put("phoneNumber","123123123");
        jsonObject.put("stateId",1);
        jsonObject.put("typeId",2);


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        Incidence response =
                restTemplate.postForObject("/incidences/new", request, Incidence.class);

        System.out.println("--------GET: "+ response.getId());



        assertNotNull(response);
        assert(response.getDescriptionText().equals("testTEXT"));
        assert(response.getState().getId().equals(1));
    }

   /* @Test
    public void putIncidences() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", 3);
        jsonObject.put("coordinates","-25.959779;32.578845");
        jsonObject.put("email","test@test");
        jsonObject.put("severityId",1);
        jsonObject.put("descriptionText","text-edited");
        jsonObject.put("phoneNumber","123123123");
        jsonObject.put("stateId",1);
        jsonObject.put("typeId",2);


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> prePutResponse = restTemplate.getForEntity("/incidences", String.class);

        restTemplate.put("/incidences/edit", request, Incidence.class);

        ResponseEntity<String> afterPutResponse = restTemplate.getForEntity("/incidences", String.class);

        assert(!prePutResponse.getBody().equals(afterPutResponse.getBody()));

    }*/

}
