
INSERT INTO institutions (id,name,email,phone_number) VALUES (1,'administration','admin@email.com','931234567');
INSERT INTO institutions (id,name,email,phone_number) VALUES (5,'ionic','ionic@pionic.com','111222');
INSERT INTO institutions (id,name,email,phone_number) VALUES (27,'bomberos','fuego@humo.com','080');
INSERT INTO institutions (id,name,email,phone_number) VALUES (38,'Policia','arroba@policia.edited-again','111222');
INSERT INTO institutions (id,name,email,phone_number) VALUES (40,'bomberos2','fuego@humo.new','080');
INSERT INTO institutions (id,name,email,phone_number) VALUES (41,'bomberos','fuego@humo.com','080');
INSERT INTO institutions (id,name,email,phone_number) VALUES (42,'bomberos','fuego@humo.com','080');
INSERT INTO institutions (id,name,email,phone_number) VALUES (46,'amamama','fuego@humo.com','080');

INSERT INTO severities (id, name) VALUES (0,'LOW');
INSERT INTO severities (id, name) VALUES (1,'MEDIUM');
INSERT INTO severities (id, name) VALUES (2,'HIGH');

INSERT INTO states (id, state_name) VALUES (0,'Pendiente de verificación');
INSERT INTO states (id, state_name) VALUES (1,'Abierta');
INSERT INTO states (id, state_name) VALUES (2,'En progreso');
INSERT INTO states (id, state_name) VALUES (3,'Resuelta');
INSERT INTO states (id, state_name) VALUES (4,'Denegada');

INSERT INTO incidence_types (id, name, emergency_institution_id) VALUES (1,'Fuego',27);
INSERT INTO incidence_types (id, name, emergency_institution_id) VALUES (2,'type-0',NULL);

INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (1,'2020-01-01 00:00:01','2020-05-24 23:27:43',3,2,'PUT incidences desde postman y aws','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (4,'2020-01-02 00:00:01','2020-02-02 00:00:01',0,2,'sec-text2-aws','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (5,'2020-05-20 23:57:09','2020-05-20 23:57:09',0,2,'POST in postman','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (6,'2020-05-21 19:52:26','2020-05-21 19:52:26',0,2,'POST in postman','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (8,'2020-05-21 21:29:52','2020-05-21 21:29:52',0,2,'POST in postman','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (9,'2020-05-21 21:33:02','2020-05-21 21:33:02',0,2,'POST in postman','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (10,'2020-05-21 21:33:27','2020-05-21 21:33:27',0,2,'POST in postman','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (11,'2020-05-22 00:26:18','2020-05-22 00:26:18',0,2,'POST in postman','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (12,'2020-05-22 00:41:29','2020-05-22 00:41:29',0,2,'ionic create ','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (13,'2020-05-22 00:41:29','2020-05-22 00:41:29',0,2,'ionic create ','-25.959779;32.578845',0);
INSERT INTO incidences (id, creation_date, last_update, state_id, type_id, description_text, coordinates, severity_id) VALUES (14,'2020-05-22 12:20:13','2020-05-22 12:20:13',0,2,'POST incidences/new','-25.959779;32.578845',0);

INSERT INTO users (id, username, password, super_admin) VALUES (4,'admin2','$2a$10$Yb2SQVCclymcVnP8UzWVT.GPKsGznD151sZqVG.U7WDKhvL.og1NC',1);
INSERT INTO users (id, username, password, email, super_admin) VALUES (5,'admin3','$2a$10$/3nXwL3pGcVgX7PKIcwjC.yvvDvKNwNF5r1nZNuIoedigZA95vyzS','amdin3@email.com',0);
INSERT INTO users (id, username, password, email, super_admin) VALUES (7,'pepito','$2a$10$rT.xt4htBQIjV/8L12Aif.3J9sVnRu44ALEHHSptbHMpoM1dd/9xu','pepito@grillo.com',1);
INSERT INTO users (id, username, password, email, super_admin) VALUES (8,'pepito1','$2a$10$m.wQUWyVEmAo./0PbF1f7uxJdTX4M8inEiPW5hYGxt9OOcLQ5ygRK','pepito@grillo1.com',0);
INSERT INTO users (id, username, password, email, super_admin) VALUES (10,'pepito2','$2a$10$Ht3FuDKCS/IWmDJl6wpf8OvEMho.LsrqFe0U0C5V7ZtokWxaW.Lk6','pepito@grillo2.com',0);
INSERT INTO users (id, username, password, email, super_admin) VALUES (12,'pepito3','$2a$10$k6VMoaRahbig1AJpO17FbO.3kip1sdngdtlNnVOxVNHggn3aIJN6i','pepito@grillo3.com',0);
INSERT INTO users (id, username, password, email, super_admin) VALUES (14,'admin','$2a$10$VHUb6SyC35UbO5yAEVSfC.zQixcTgFnV1gxGmdsw2Ofcjjucitby.','amdin@email.com',1);

INSERT INTO files (id, file_path) VALUES (1, 'path/file');

INSERT INTO incidence_files (file_id, incidence_id) values (1, 1);

INSERT INTO institution_competences (institution_id, type_id) VALUES (1,2);
INSERT INTO institution_competences (institution_id, type_id) VALUES (27,2);
INSERT INTO institution_competences (institution_id, type_id) VALUES (38,2);
INSERT INTO institution_competences (institution_id, type_id) VALUES (40,2);
INSERT INTO institution_competences (institution_id, type_id) VALUES (41,2);
INSERT INTO institution_competences (institution_id, type_id) VALUES (42,2);
INSERT INTO institution_competences (institution_id, type_id) VALUES (46,2);
